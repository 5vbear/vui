/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * @ignore
 * @class
 */
VUI.Class('Validate',{
	OPTS:$.extend({
		rule:{}
		,msgId:''
		,errorMsg:''
		,successMsg:''
		// 自定义校验方法
		,validateHandler:null
	}, VUI.Config.Validate)
	
	/**
	 * 构造函数
	 * @private
	 */
	,init:function(opts) {
		this.opts = $.extend({},this.OPTS, opts);
		this.setMsgId(this.opts.msgId);
	}
	/**
	 * 设置提示信息的id
	 */
	,setMsgId:function(id){
		this.opts.msgId = id;
		this.$msgDiv = $('#'+this.opts.msgId);
	}
	,showErrorMsg:function(msg) {
		if(this.opts.errorClass) {
			this.$msgDiv.removeClass().addClass(this.opts.errorClass);
		}
		this.$msgDiv.html(msg || this.opts.errorMsg);
	}
	,showSuccessMsg:function(msg) {
		if(this.opts.successClass) {
			this.$msgDiv.removeClass().addClass(this.opts.successClass);
		}
		this.$msgDiv.html(msg || this.opts.successMsg);
	}
	/**
	 * 验证操作
	 */
	,validate:function(val){
		if(typeof this.opts.validateHandler === 'function') {
			var succ =  this.opts.validateHandler(val);
			if(!succ){
				this.showErrorMsg();
			}
			return succ;
		}
		var rule = this.opts.rule;
		for(var key in rule) {
			// 检测空值
			var len = this.getValueLength(val);
			if(len === 0) {
				if(this.opts.rule.notNull) {
					this.showErrorMsg();
					return false;
				}
				return true;
			}
			
			// 检测长度
			if(!this.validateLength(val)) {
				return false;
			}
			
			var shouldValidate = rule[key];
			if(shouldValidate && !this.validateItem(val,key)) {
				return false;
			}
		}
		// 验证成功
		this.showSuccessMsg();
		return true;
	}
	/**
	 * 验证长度
	 */
	,validateLength:function(val) {
		var len = this.getValueLength(val);
		var mixLen = this.opts.rule.minLength || -1;
		var maxLen = this.opts.rule.maxLength || -1;
		if(mixLen !== -1) {
			if(len < mixLen) {
				this.showErrorMsg();
				return false;
			}
		}
		
		if(maxLen !== -1) {
			if(len > maxLen) {
				this.showErrorMsg();
				return false;
			}
		}
		
		return true;
	}
	/**
	 * 验证单个项
	 */
	,validateItem:function(val,item) {
		var validateMap = VUI.ValidateStore;
		var validateHandler = validateMap[item];
		if($.isFunction(validateHandler)) {
			// 验证不通过
			if(!validateHandler(val)) {
				this.showErrorMsg();
				return false;
			}
		}
		return true;
	}
	/**
	 * 返回字节长度,一个汉字返回2个长度
	 */
	,getValueLength:function(value) {
		if(typeof value === 'undefined') {
			return 0;
		}
		// 如果是数组
		if($.isArray(value)) {
			return value.length;
		}
		return (value + '').replace(/[^\x00-\xff]/g, "**").length;
	}
});

/**
 * @ignore
 */
VUI.ValidateStore = (function(){
	var reg_positiveInt = /^[1-9]\d*$/
		,reg_naturalNum = /^(0|([1-9]\d*))$/
		,reg_positiveNum = /^(((0|([1-9]\d*))[\.]?[0-9]+)|[1-9])$/
		,reg_floatNum = /^-?[0-9]{1,4}([.]{1}[0-9]{1,})?$/
		,reg_positiveFloatNum = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/
		,reg_notNegativeFloatNum = /^[0-9]{1,4}([.]{1}[0-9]{1,})?$/
		,reg_email = /^\s*[_a-zA-Z0-9\-]+(\.[_a-zA-Z0-9\-]*)*@[a-zA-Z0-9\-]+([\.][a-zA-Z0-9\-]+)+\s*$/
		,reg_mobile = /^\s*(\+86)*0*1(3|5|8)\d{9}\s*$/
		,reg_tel = /\(?0\d{2}[) -]?\d{8}/
	
	return {
		/**
		 * 匹配正整数
		 */
		positiveInt:function(val) {
			return reg_positiveInt.test(val);
		}
		/**
		 * 匹配自然数,0,1,2,3...
		 */
		,naturalNum:function(val) {
			return reg_naturalNum.test(val);
		}
		/**
		 * 匹配正数
		 */ 
		,positiveNum:function(val) {
			return reg_positiveNum.test(val);
		}
		/**
		 * 匹配浮点数
		 */
		,floatNum:function(val) {
			return reg_floatNum.test(val);
		}
		/**
		 * 匹配正浮点数
		 */
		,positiveFloatNum:function(val) {
			return reg_positiveFloatNum.test(val);
		}
		/**
		 * 匹配非负浮点数（正浮点数 + 0） 
		 */
		,notNegativeFloatNum:function(val) {
			if(val === 0) {
				return true;
			}
			return reg_notNegativeFloatNum.test(val);
		}
		/**
		 * 匹配邮箱
		 */
		,email:function(val) {
			return reg_email.test(val);
		}
		/**
		 * 匹配手机号
		 */
		,mobile:function(val) {
			return reg_mobile.test(val);
		}
		/**
		 * 匹配联系电话
		 * (010)88886666，或022-22334455，或02912345678
		 */
		,tel:function(val) {
			return reg_tel.test(val);
		}
	};
})();

})();