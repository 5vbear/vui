/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * 日历控件
 * @class VUI.Calendar
 * @extends VUI.Component
 */
VUI.Class('Calendar',{
	// 默认属性
	OPTS:{
		/**
		 * @cfg {Boolean} showFoot 是否显示底部按钮部分
		 */
		showFoot:false
		/**
		 * @cfg {Function} clickHandler 点击日期触发的事件
		 */
		,clickHandler:function(dayData){}
	}
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
	}
	//@override
	,getViewClass:function() {
		return VUI.CalendarView;
	}
	,visiable:function() {
		return this.view.visiable();
	}
	,showCalendar:function(date) {
		this.view.showCalendar(date);
	}
},VUI.Component);

})();